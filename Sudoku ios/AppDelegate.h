//
//  AppDelegate.h
//  Sudoku ios
//
//  Created by Philip Huffman on 2015-06-30.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Game.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

