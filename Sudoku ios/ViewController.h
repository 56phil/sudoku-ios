//
//  ViewController.h
//  Sudoku ios
//
//  Created by Philip Huffman on 2015-06-30.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Game.h"
#import "GameSquare.h"

enum gameState {
    tooFewUnassignedCells,
    tooManyUnassignedCells,
    inProgress,
    invalidBoard,
    custom,
    canned,
    clearGame,
    saved,
    solved,
    dataError
};

@interface ViewController : UIViewController <UIGestureRecognizerDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) NSMutableArray *board;
@property (strong, nonatomic) NSDate *startTime;
@property (nonatomic) NSTimeInterval solveTime;
@property (strong, nonatomic) NSDateComponentsFormatter *timeIntervalFormatter;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) Game *currentGame;

@property (nonatomic) enum gameState state;
//@property (nonatomic) long currentGameIndex;

@property (strong, nonatomic) UIColor *black;
@property (strong, nonatomic) UIColor *green;
@property (strong, nonatomic) UIColor *red;
@property (strong, nonatomic) UIColor *white;
@property (strong, nonatomic) UIColor *cellBackgroundColor;

@property (strong, nonatomic) NSArray *textFields;
@property (strong, nonatomic) NSNumberFormatter *numberFormatter;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGestureRecognizer;

@property (weak, nonatomic) IBOutlet UIButton *solveButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *freshGameButton;
@property (weak, nonatomic) IBOutlet UITextField *gameName;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UITextField *c11;
@property (weak, nonatomic) IBOutlet UITextField *c12;
@property (weak, nonatomic) IBOutlet UITextField *c13;
@property (weak, nonatomic) IBOutlet UITextField *c14;
@property (weak, nonatomic) IBOutlet UITextField *c15;
@property (weak, nonatomic) IBOutlet UITextField *c16;
@property (weak, nonatomic) IBOutlet UITextField *c17;
@property (weak, nonatomic) IBOutlet UITextField *c18;
@property (weak, nonatomic) IBOutlet UITextField *c19;
@property (weak, nonatomic) IBOutlet UITextField *c21;
@property (weak, nonatomic) IBOutlet UITextField *c22;
@property (weak, nonatomic) IBOutlet UITextField *c23;
@property (weak, nonatomic) IBOutlet UITextField *c24;
@property (weak, nonatomic) IBOutlet UITextField *c25;
@property (weak, nonatomic) IBOutlet UITextField *c26;
@property (weak, nonatomic) IBOutlet UITextField *c27;
@property (weak, nonatomic) IBOutlet UITextField *c28;
@property (weak, nonatomic) IBOutlet UITextField *c29;
@property (weak, nonatomic) IBOutlet UITextField *c31;
@property (weak, nonatomic) IBOutlet UITextField *c32;
@property (weak, nonatomic) IBOutlet UITextField *c33;
@property (weak, nonatomic) IBOutlet UITextField *c34;
@property (weak, nonatomic) IBOutlet UITextField *c35;
@property (weak, nonatomic) IBOutlet UITextField *c36;
@property (weak, nonatomic) IBOutlet UITextField *c37;
@property (weak, nonatomic) IBOutlet UITextField *c38;
@property (weak, nonatomic) IBOutlet UITextField *c39;
@property (weak, nonatomic) IBOutlet UITextField *c41;
@property (weak, nonatomic) IBOutlet UITextField *c42;
@property (weak, nonatomic) IBOutlet UITextField *c43;
@property (weak, nonatomic) IBOutlet UITextField *c44;
@property (weak, nonatomic) IBOutlet UITextField *c45;
@property (weak, nonatomic) IBOutlet UITextField *c46;
@property (weak, nonatomic) IBOutlet UITextField *c47;
@property (weak, nonatomic) IBOutlet UITextField *c48;
@property (weak, nonatomic) IBOutlet UITextField *c49;
@property (weak, nonatomic) IBOutlet UITextField *c51;
@property (weak, nonatomic) IBOutlet UITextField *c52;
@property (weak, nonatomic) IBOutlet UITextField *c53;
@property (weak, nonatomic) IBOutlet UITextField *c54;
@property (weak, nonatomic) IBOutlet UITextField *c55;
@property (weak, nonatomic) IBOutlet UITextField *c56;
@property (weak, nonatomic) IBOutlet UITextField *c57;
@property (weak, nonatomic) IBOutlet UITextField *c58;
@property (weak, nonatomic) IBOutlet UITextField *c59;
@property (weak, nonatomic) IBOutlet UITextField *c62;
@property (weak, nonatomic) IBOutlet UITextField *c61;
@property (weak, nonatomic) IBOutlet UITextField *c63;
@property (weak, nonatomic) IBOutlet UITextField *c64;
@property (weak, nonatomic) IBOutlet UITextField *c65;
@property (weak, nonatomic) IBOutlet UITextField *c66;
@property (weak, nonatomic) IBOutlet UITextField *c67;
@property (weak, nonatomic) IBOutlet UITextField *c68;
@property (weak, nonatomic) IBOutlet UITextField *c69;
@property (weak, nonatomic) IBOutlet UITextField *c71;
@property (weak, nonatomic) IBOutlet UITextField *c72;
@property (weak, nonatomic) IBOutlet UITextField *c73;
@property (weak, nonatomic) IBOutlet UITextField *c74;
@property (weak, nonatomic) IBOutlet UITextField *c75;
@property (weak, nonatomic) IBOutlet UITextField *c76;
@property (weak, nonatomic) IBOutlet UITextField *c77;
@property (weak, nonatomic) IBOutlet UITextField *c78;
@property (weak, nonatomic) IBOutlet UITextField *c79;
@property (weak, nonatomic) IBOutlet UITextField *c81;
@property (weak, nonatomic) IBOutlet UITextField *c82;
@property (weak, nonatomic) IBOutlet UITextField *c83;
@property (weak, nonatomic) IBOutlet UITextField *c84;
@property (weak, nonatomic) IBOutlet UITextField *c85;
@property (weak, nonatomic) IBOutlet UITextField *c86;
@property (weak, nonatomic) IBOutlet UITextField *c87;
@property (weak, nonatomic) IBOutlet UITextField *c88;
@property (weak, nonatomic) IBOutlet UITextField *c89;
@property (weak, nonatomic) IBOutlet UITextField *c91;
@property (weak, nonatomic) IBOutlet UITextField *c92;
@property (weak, nonatomic) IBOutlet UITextField *c93;
@property (weak, nonatomic) IBOutlet UITextField *c94;
@property (weak, nonatomic) IBOutlet UITextField *c95;
@property (weak, nonatomic) IBOutlet UITextField *c96;
@property (weak, nonatomic) IBOutlet UITextField *c97;
@property (weak, nonatomic) IBOutlet UITextField *c98;
@property (weak, nonatomic) IBOutlet UITextField *c99;

@end
