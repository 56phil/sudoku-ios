//
//  GameSquare.h
//  Pods
//
//  Created by Philip Huffman on 2015-07-10.
//
//

#import <Foundation/Foundation.h>

@interface GameSquare : NSObject
@property (strong, nonatomic) UIColor *digitColor;
@property (nonatomic) unsigned originalValue;
@property (nonatomic) unsigned currentValue;
@end
