//
//  Game.m
//  
//
//  Created by Philip Huffman on 2015-08-08.
//
//

#import "Game.h"


@implementation Game

@dynamic game;
@dynamic index;
@dynamic lastPlayed;
@dynamic bestTime;
@dynamic bestTimeDevice;
@dynamic bestTimeDate;

@end
