//  Sudoku-Controller.m
//  Sudoku C
//
//  Created by Philip Huffman on 2013-09-13.
//  Copyright (c) 2013 Philip Huffman. All rights reserved.

#import "SudokuController.h"

@implementation SudokuController

-(void)awakeFromNib{
//    textFields = @[c11,c12,c13,c14,c15,c16,c17,c18,c19,
//                   c21,c22,c23,c24,c25,c26,c27,c28,c29,
//                   c31,c32,c33,c34,c35,c36,c37,c38,c39,
//                   c41,c42,c43,c44,c45,c46,c47,c48,c49,
//                   c51,c52,c53,c54,c55,c56,c57,c58,c59,
//                   c61,c62,c63,c64,c65,c66,c67,c68,c69,
//                   c71,c72,c73,c74,c75,c76,c77,c78,c79,
//                   c81,c82,c83,c84,c85,c86,c87,c88,c89,
//                   c91,c92,c93,c94,c95,c96,c97,c98,c99];
//    
//    err = [NSSound soundNamed:@"Basso"];
//    frg = [NSSound soundNamed:@"Frog"];
//    png = [NSSound soundNamed:@"Ping"];
//    srt = [NSSound soundNamed:@"Glass"];
    
    blk = [UIColor blackColor];
    grn = [UIColor greenColor];
    red = [UIColor redColor];
    wht = [UIColor whiteColor];
    
//    [_sound setState:NSOffState];
    
//    for (NSTextField *t in textFields) {
//        [t setBackgroundColor:blk];
//    }
    
    sysCalendar = [NSCalendar currentCalendar];
    srandom((int)time(0));
    
    [self updateDisplay];
}

/*
 read games from file, seed RNG, call new game
 */
-(SudokuController *)init{
    self = [super init];
    if (self) {
        self.fileName = [@"~/dev/Games.xml" stringByExpandingTildeInPath];
        games = [[NSMutableArray alloc]initWithContentsOfFile:fileName];
        self.indexToGame = games.count - 1;
        [self fetchNewGameUsingIndexToGame];
        [self setBeginTime:[NSDate date]];
    }
    return self;
}

-(IBAction)save:(id)sender{
    self.copyGameToOriginal = YES;
    [self updateGameArray];
    int count = [self countOfUnassignedCells];
    if (count > 73) {
        self.state = tooManyUnassignedCells;
    } else if (count < 18) {
        self.state = tooFewUnassignedCells;
    } else if (self.state == clearGame && [self ValidateGameBoard]) {
        [self saveCustomGame];
        [self setState:saved];
    }
    
    switch ([self state]) {
        case canned:
            [self setState:alreadySaved];
            break;
            
        case saved:
            [self solveNextUnassignedPosition];
            break;
            
        default:
            break;
    }
    
    [self updateDisplay];
}

-(IBAction)knew:(id)sender {
    if ([name integerValue] == [self indexToGame]) {
        [self newRandomlySelectedGame];
    } else {
        [self setIndexToGame:[name integerValue]];
        [self fetchNewGameUsingIndexToGame];
    }
    [self updateDisplay];
}

-(IBAction)clear:(id)sender{
    [self clearCurrentGame];
    [self updateDisplay];
}

-(IBAction)solve:(id)sender{
    if ([_sound state] == NSOnState) {
        [srt play];
    }
    
    if ([self countOfUnassignedCells] == 0) {
        [self fetchNewGameUsingIndexToGame];
    }
    
    [self setStartTime:[NSDate date]];
    [self setBeginTime:startTime];
    [self updateTime];
    
    switch ([self state]) {
        case canned:
        case custom:
        case saved:
            [self solveNextUnassignedPosition];
            break;
            
        default:
            [self setCopyGameToOriginal:YES];
            [self updateGameArray];
            if ([self ValidateGameBoard]) {
                [self setState:custom];
                [self solveNextUnassignedPosition];
            } else {
                [self setState:invalidBoard];
            }
            break;
    }
    [self updateDisplay];
    [self createNewGame];
}

-(void)updateDisplay{
    [clearButton setState:NSOnState];
    [knewButton setState:NSOnState];
    [saveButton setState:NSOffState];
    [solveButton setState:NSOnState];
    
    switch ([self state]) {
        case alreadySaved:
            [name setStringValue:@"Aready SAVED!"];
            [name setTextColor:red];
            if ([_sound state] == NSOnState) {
                [err play];
            }
            break;
            
        case canned:
            [name setIntegerValue:[self indexToGame]];
            [name setTextColor:grn];
            if ([_sound state] == NSOnState) {
                [png play];
            }
            break;
            
        case clear:
            [name setStringValue:@" "];
            [saveButton setState:NSOnState];
            if ([_sound state] == NSOnState) {
                [srt play];
            }
            break;
            
        case custom:
            [name setStringValue:@"Custom"];
            [name setTextColor:wht];
            if ([_sound state] == NSOnState) {
                [png play];
            }
            break;
            
        case invalidBoard:
            [name setStringValue:@"Invalid"];
            [name setTextColor:red];
            [solveButton setState:NSOffState];
            if ([_sound state] == NSOnState) {
                [err play];
            }
            break;
            
        case saved:
            [name setIntegerValue:[self indexToGame]];
            [name setTextColor:wht];
            if ([_sound state] == NSOnState) {
                [png play];
            }
            break;
            
        case tooFewUnassignedCells:
            [name setStringValue:@"NOT ENOUGH EMPTY CELLS"];
            [name setTextColor:red];
            if ([_sound state] == NSOnState) {
                [err play];
            }
            break;
            
        case tooManyUnassignedCells:
            [name setStringValue:@"TOO MANY EMPTY CELLS"];
            [name setTextColor:red];
            if ([_sound state] == NSOnState) {
                [err play];
            }
            break;
    }
    
    [self updateBoard];
    [self updateTime];
    [self setStartTime:nil];
}

-(void)updateTime {
    NSDateComponents *conversionInfo;
    unsigned int unitFlags = NSSecondCalendarUnit | NSMinuteCalendarUnit | NSHourCalendarUnit;
    [dTime setStringValue:@" "];
    if ([self startTime] != nil) {
        conversionInfo = [sysCalendar components:unitFlags fromDate:startTime  toDate:[NSDate date]  options:0];
        if ([conversionInfo hour] > 0 || [conversionInfo minute] > 0) {
            [dTime setStringValue:[NSString stringWithFormat:@"Run time: %ld:%02ld:%02ld", (long)[conversionInfo hour], (long)[conversionInfo minute], (long)[conversionInfo second]]];
        }
        else if ([conversionInfo second] == 1) {
            [dTime setStringValue:[NSString stringWithFormat:@"Run time: 1 second."]];
        }
        else {
            [dTime setStringValue:[NSString stringWithFormat:@"Run time: %ld seconds.", (long)[conversionInfo second]]];
        }
    }
    
    conversionInfo = [sysCalendar components:unitFlags fromDate:[NSDate date]];
    [cTime setStringValue:[NSString stringWithFormat:@"Time: %ld:%02ld:%02ld", [conversionInfo hour], [conversionInfo minute], [conversionInfo second]]];
    
    conversionInfo = [sysCalendar components:unitFlags fromDate:beginTime];
    [sTime setStringValue:[NSString stringWithFormat:@"Start Time: %ld:%02ld:%02ld", [conversionInfo hour], [conversionInfo minute], [conversionInfo second]]];
    
    [cTime display];
    [dTime display];
    [sTime display];
}

-(void)updateBoard {
    int *c = [self gameStartAddress];
    int *o = [self originalStartAddress];
    
    for (NSTextField *t in textFields) {
        if (*c) {
            if (*o == 0) {
                [t setTextColor:grn];
            } else if (*o > 0) {
                [t setTextColor:wht];
            } else {
                [t setTextColor:red];
            }
            [t setIntegerValue:*c];
        } else {
            [t setStringValue:@" "];
            [t setTextColor:grn];
        }
        if (*o == 0) {
            [t display];
        }
        c++;
        o++;
    }
    [self updateTime];
}

/*
 set up game
 */
-(void)updateGameArray {
    int *c = [self gameStartAddress];
    
    for (NSTextField *t in textFields) {
        *c++ = (int)[t integerValue];
    }
    
    if ([self copyGameToOriginal]) {
        [self copyToOriginal];
    }
}

-(int *)originalStartAddress{
    return originalBoard;
}

-(int *)gameStartAddress{
    return gameBoard;
}

/*
 Randomly select an index to games
 */
-(void)newRandomlySelectedGame {
    [self setIndexToGame:random() % [games count]];
    [self fetchNewGameUsingIndexToGame];
}

/*
 extract the game & put it in play.
 */
-(void)fetchNewGameUsingIndexToGame {
    [self clearCurrentGame];
    
    if ([self indexToGame] >= [games count]) {
        [self setIndexToGame:[games count] - 1];
    }
    
    NSArray *game = [games[[self indexToGame]]componentsSeparatedByString:@","];
    int *c = [self gameStartAddress];
    
    for (NSString *s in game) {
        *c++ = [s intValue];
    }
    
    [self copyToOriginal];
    
    if (![self ValidateGameBoard]) {
        [self removeCurrentGame];
        [self newRandomlySelectedGame];
    }
    
    [self setCopyGameToOriginal:NO];
    [self setState:canned];
}

/*
 take a solved game, remove most of the solution, put it into string format, add the string to Games and save it
 */
-(void)createNewGame {
    if ([games count] < 0x1000 && [self ValidateGameBoard]) {
        NSMutableString *str = [[NSMutableString alloc]init];
        int *c = [self gameStartAddress];
        int *e = c + 81;
        
        if (random() % 5) {
            int lim = random() % 250 + 550;
            while (c < e){
                if (random() % 1000 > lim) {
                    [str appendFormat:@"%d,", *c];
                } else {
                    [str appendString:@"0,"];
                }
                c++;
            }
        } else {
            int n = random() % 9 + 1;
            while (c < e){
                if (*c == n) {
                    [str appendFormat:@"%d,", n];
                } else {
                    [str appendString:@"0,"];
                }
                c++;
            }
        }
        
        [str appendString:@"="];
        NSRange daEnd = [str rangeOfString:@",="];
        [str deleteCharactersInRange:daEnd];
        [games addObject:str];
        [self saveGamesArray];
    }
}

-(void)saveCustomGame{
    NSMutableString *str = [[NSMutableString alloc]init];
    int *c = [self gameStartAddress];
    int *e = c + 81;
    
    while (c < e) {
        [str appendFormat:@"%d,", *c++];
    }
    
    [str appendString:@"="];
    NSRange daEnd = [str rangeOfString:@",="];
    [str deleteCharactersInRange:daEnd];
    [games addObject:str];
    [self setIndexToGame:[games count] - 1];
    [self saveGamesArray];
}

/*
 remove current game from array
 */
-(void)removeCurrentGame{
    if (indexToGame < [games count]) {
        [games removeObjectAtIndex:indexToGame];
        [self saveGamesArray];
    }
}

/*
 write games to file
 */
-(void)saveGamesArray{
    if ([self countOfUnassignedCells] < 81) {
        [games writeToFile:[self fileName]
                atomically:YES];
    }
}

/*
 recursive backtracking algorithm
 */
-(BOOL)solveNextUnassignedPosition {
    if (([_dRate maxValue] > [_dRate floatValue]) && !(random() % (long)[_dRate floatValue])) {
        [self updateBoard];
    }
    
    int *daEnd = [self gameStartAddress] + 81;
    
    for (int *currentAddress = [self gameStartAddress]; currentAddress < daEnd; currentAddress++) {
        if (*currentAddress == 0) {
            for (int prospect = 1; prospect < 10; prospect++) {
                if ([self noConflictAtPosition:currentAddress
                                 usingProspect:prospect]) {
                    *currentAddress = prospect;
                    if ([self solveNextUnassignedPosition]) return YES;
                }
            }
            *currentAddress = 0;
            return NO;
        }
    }
    return YES;
}

/*
 check out board
 */
-(BOOL)ValidateGameBoard{
    int *c = [self gameStartAddress];
    int *o = [self originalStartAddress];
    int *e = c + 81;
    int p;
    BOOL hasError = NO;
    
    while (c < e) {
        if (*c) {
            p = *c;
            *c = 0;
            if (p < 1 || p > 9 || ![self noConflictAtPosition:c usingProspect:p]) {
                hasError = YES;
                *o = p < 1 ? p : -p;
            }
            *c = p;
        }
        o++;
        c++;
    }
    
    if ([self state] == clear || [self state] == invalidBoard) {
        [self setState:custom];
    }
    
    if (hasError) {
        [self setState:invalidBoard];
    }
    
    [self setCopyGameToOriginal:NO];
    
    return !hasError;
}

/*
 Search for dupes in column, row and subgrid
 */
-(BOOL)noConflictAtPosition:(int *)position
              usingProspect:(int)prospect {
    
    int *firstElement = [self gameStartAddress];
    int *arrayEnd = firstElement + 81;
    unsigned long index = position - firstElement;
    
    for (int *currentAddress = firstElement + index % 9; currentAddress < arrayEnd; currentAddress += 9) {
        if (*currentAddress == prospect) return NO;
    }
    
    int *rowEnd = firstElement + index / 9 * 9 + 9;
    for (int *currentAddress = rowEnd - 9; currentAddress < rowEnd; currentAddress++) {
        if (*currentAddress == prospect) return NO;
    }
    
    int *upperLeft = firstElement + index / 27 * 27 + index % 9 / 3 * 3;
    int *lowerRight = upperLeft + 21;
    int c = 1;
    for (int *currentAddress = upperLeft; currentAddress < lowerRight; currentAddress++) {
        if (*currentAddress == prospect) return NO;
        if (c++ % 3 == 0) currentAddress += 6;
    }
    return YES;
}

-(void)clearCurrentGame{
    int *c = [self gameStartAddress];
    int *o = [self originalStartAddress];
    int *e = c + 81;
    
    while (c < e) {
        *c++ = 0;
        *o++ = 0;
    }
    
    [self setCopyGameToOriginal:YES];
    [self setState:clear];
}

-(int)countOfUnassignedCells {
    int count = 0;
    int *c = [self gameStartAddress];
    int *e = c + 81;
    
    while (c < e) {
        if (*c++ == 0) count++;
    }
    return count;
}

-(void)copyToOriginal {
    int *o = [self originalStartAddress];
    int *c = [self gameStartAddress];
    int *e = c + 81;
    
    while (c < e) {
        *o++ = *c++;
    }
}

@end
