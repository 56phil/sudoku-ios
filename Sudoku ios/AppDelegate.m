//
//  AppDelegate.m
//  Sudoku ios
//
//  Created by Philip Huffman on 2015-06-30.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    NSDictionary *originalPreferences = @{@"number_of_saved_games_max" : @"250",
                                          @"unassigned_cells_min" : @"33",
                                          @"unassigned_cells_max" : @"72"};
    NSUserDefaults *userpreferences = [NSUserDefaults standardUserDefaults];
    [userpreferences setValuesForKeysWithDictionary:originalPreferences];
    [userpreferences registerDefaults:originalPreferences];
    [userpreferences synchronize];
    
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"SudokuGameStore"];
    
    /* initialization
     */
    if ([Game MR_countOfEntitiesWithContext:[NSManagedObjectContext MR_defaultContext]] == 0) {
        NSArray *games = [[NSArray alloc]initWithContentsOfFile:@"/Users/a56phil/dev/Games.xml"];
        int index = 0;
        for (NSString *game in games) {
            Game *nextGame = [Game MR_createEntity];
            nextGame.game = game;
            nextGame.bestTime = @0.0;
            nextGame.lastPlayed = nil;
            nextGame.bestTimeDate = nil;
            nextGame.bestTimeDevice = nil;
            nextGame.index = [[NSNumber alloc] initWithInt:index++];
        }
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *savedGamesMax = [defaults stringForKey:@"number_of_saved_games_max"];
    if ([Game MR_countOfEntitiesWithContext:[NSManagedObjectContext MR_defaultContext]] > savedGamesMax.intValue) {
        [Game MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"index.intValue >= %d", savedGamesMax.intValue]
                                  inContext:[NSManagedObjectContext MR_defaultContext]];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
