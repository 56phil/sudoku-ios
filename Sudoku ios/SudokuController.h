//
//  Sudoku-Controller.h
//  Sudoku C
//
//  Created by Philip Huffman on 2013-09-13.
//  Copyright (c) 2013 Philip Huffman. All rights reserved.
//

#import <UIKit/UIKit.h>

enum gameState {
  alreadySaved,
  tooFewUnassignedCells,
  tooManyUnassignedCells,
  invalidBoard,
  custom,
  canned,
  clearGame,
  saved
};

@interface SudokuController : NSObject {
  NSArray *textFields;
  NSCalendar *sysCalendar;
  
  NSDate *startTime;
    
//  NSSound *err;
//  NSSound *png;
//  NSSound *srt;
//  NSSound *frg;
  
  UIColor *blk;
  UIColor *grn;
  UIColor *red;
  UIColor *wht;
  
  NSMutableArray *games;
  NSString *fileName;
  unsigned long indexToGame;
  int gameBoard[81];
  int originalBoard[81];
  BOOL copyGameToOrginal;
  enum gameState state;
  
//  __weak NSButton *_sound;
//  __weak NSSlider *_dRate;
//  
//  IBOutlet NSButton *knewButton;
//  IBOutlet NSButton *saveButton;
//  IBOutlet NSButton *solveButton;
//  IBOutlet NSButton *clearButton;
//  

    //  IBOutlet NSTextField *dTime;
//  IBOutlet NSTextField *sTime;
//  IBOutlet NSTextField *cTime;
}

//@property (strong, nonatomic) NSDate *startTime;
@property (strong, nonatomic) NSDate *beginTime;
//@property (strong, nonatomic) NSButton *clearButton;
//@property (strong, nonatomic) NSButton *knewButton;
//@property (strong, nonatomic) NSButton *saveButton;
//@property (strong, nonatomic) NSButton *solveButton;
//
//@property (weak, nonatomic) IBOutlet NSButton *sound;
//@property (weak, nonatomic) IBOutlet NSSlider *dRate;
//
//-(IBAction)save:(id)sender;
//-(IBAction)clear:(id)sender;
//-(IBAction)knew:(id)sender;
//-(IBAction)solve:(id)sender;
//
//-(SudokuController *)init NS_DESIGNATED_INITIALIZER;
//@property (nonatomic, readonly) int *gameStartAddress;
//@property (nonatomic, readonly) int *originalStartAddress;
//@property (nonatomic, readonly) int countOfUnassignedCells;
//@property (nonatomic, readonly) BOOL ValidateGameBoard;
//@property (nonatomic, readonly) BOOL solveNextUnassignedPosition;
-(BOOL)noConflictAtPosition:(int *)position
              usingProspect:(int)prospect;
-(void)createNewGame;
-(void)removeCurrentGame;
-(void)saveGamesArray;
-(void)fetchNewGameUsingIndexToGame;
-(void)newRandomlySelectedGame;
-(void)clearCurrentGame;
-(void)saveCustomGame;
-(void)copyToOriginal;
-(void)updateDisplay;
-(void)updateBoard;
-(void)updateTime;
-(void)updateGameArray;

@end
