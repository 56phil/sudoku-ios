//
//  ViewController.m
//  Sudoku ios
//
//  Created by Philip Huffman on 2015-06-30.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

// todo: change queuing

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) NSArray *pickerData;
@property (nonatomic) int selectedTagIndex;
@end

@implementation ViewController

- (void) setUpGestures {
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(doubleTapHandler)];
    [tapGestureRecognizer setNumberOfTapsRequired:2];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    tapGestureRecognizer.delegate = self;
}

- (void) doubleTapHandler {
    [self clearBoard];
    [self updateDisplay];
}

- (void) setUpColors {
    self.red = [UIColor redColor];
    self.black = [UIColor blackColor];
    self.green = [UIColor greenColor];
    self.white = [UIColor whiteColor];
    
    self.cellBackgroundColor = [UIColor colorWithHue:0.40
                                          saturation:0.75
                                          brightness:0.40
                                               alpha:1.0];
}

- (void) setUpTextFields {
    self.textFields = @[self.c11,self.c12,self.c13,self.c14,self.c15,self.c16,self.c17,self.c18,self.c19,
                        self.c21,self.c22,self.c23,self.c24,self.c25,self.c26,self.c27,self.c28,self.c29,
                        self.c31,self.c32,self.c33,self.c34,self.c35,self.c36,self.c37,self.c38,self.c39,
                        self.c41,self.c42,self.c43,self.c44,self.c45,self.c46,self.c47,self.c48,self.c49,
                        self.c51,self.c52,self.c53,self.c54,self.c55,self.c56,self.c57,self.c58,self.c59,
                        self.c61,self.c62,self.c63,self.c64,self.c65,self.c66,self.c67,self.c68,self.c69,
                        self.c71,self.c72,self.c73,self.c74,self.c75,self.c76,self.c77,self.c78,self.c79,
                        self.c81,self.c82,self.c83,self.c84,self.c85,self.c86,self.c87,self.c88,self.c89,
                        self.c91,self.c92,self.c93,self.c94,self.c95,self.c96,self.c97,self.c98,self.c99];
    
    int currentTag = 0;
    for (UITextField *textField in self.textFields) {
        textField.tag = ++currentTag;
        textField.delegate = self;
    }
    
    self.gameName.backgroundColor = self.cellBackgroundColor;
    self.gameName.delegate = self;
}

- (void) setUpPicker {
    self.pickerData = @[@"Delete", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9"];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    self.pickerView.hidden = YES;
}

- (void) setUpFormatters {
    self.timeIntervalFormatter = [[NSDateComponentsFormatter alloc] init];
    self.numberFormatter = [[NSNumberFormatter alloc] init];
    self.numberFormatter.usesGroupingSeparator = YES;
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    self.dateFormatter.timeStyle = NSDateFormatterShortStyle;
}

- (void) setUpFirstGame {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int savedGamesMax = [[defaults stringForKey:@"number_of_saved_games_max"]intValue];
    
    self.selectedTagIndex = -1;
    
    if ([Game MR_countOfEntities] < savedGamesMax) {
        [self loadThisGame:[Game MR_countOfEntitiesWithContext:[NSManagedObjectContext MR_defaultContext]] - 1];
    } else {
        [self loadRandomlySelectedGame];
    }
}

- (void) viewDidLoad {
    [super viewDidLoad];
    self.board = [[NSMutableArray alloc] init];
    
    [self setUpGestures];
    [self setUpColors];
    [self setUpTextFields];
    [self setUpPicker];
    [self setUpFormatters];
    [self setUpFirstGame];
    [self updateDisplay];
    
    self.startTime = [[NSDate alloc] init];
}

- (IBAction) freshGameButtonPressed:(id)sender {
    self.selectedTagIndex = -1;
    [self loadRandomlySelectedGame];
    [self updateDisplay];
}

- (IBAction) saveWasPressed:(id)sender {
    if (self.state == saved || self.state == canned) {
        [self doAlertWithtitle:@"Save not performed" andMessage:@"This game is in the database."];
    } else if (self.state == invalidBoard ||
               self.state == tooFewUnassignedCells ||
               self.state == tooManyUnassignedCells) {
        [self doAlertWithtitle:@"Save not performed" andMessage:@"The board is in an inconstant state."];
    } else {
        self.selectedTagIndex = -1;
        self.state = saved;
        [self saveGamesArray];
        [self updateDisplay];
    }
}

- (IBAction) SolveWasPressed:(id)sender {
    if ([self countOfUnassignedCells:self.board] > 0) {
        [self prepareToSolveGame];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            self.startTime = [NSDate date];
            [self solveNextUnassignedPosition];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self completeSolvedGame];
            });
        });

    } else {
        [self doAlertWithtitle:@"Puzzle Solved"
                    andMessage:@"This one is done. YOu can press \"New\" to get another game."];
    }
}

- (void) prepareToSolveGame {
    if (self.selectedTagIndex >= 0 && self.selectedTagIndex < 81) {
        UITextField *textField = [self.textFields objectAtIndex:self.selectedTagIndex];
        textField.backgroundColor = self.cellBackgroundColor;
    }
    self.selectedTagIndex = -1;
    self.solveButton.hidden = YES;
    self.saveButton.hidden = YES;
    self.freshGameButton.hidden = YES;
    self.pickerView.hidden = YES;
    [self.activityIndicator startAnimating];
}

- (void) completeSolvedGame {
    self.solveTime = -[self.startTime timeIntervalSinceNow];
    [self updateDisplay];
    [self createNewGame];
    [self.activityIndicator stopAnimating];
    self.solveButton.hidden = NO;
    self.saveButton.hidden = NO;
    self.freshGameButton.hidden = NO;
    self.state = solved;
    [self doAlertWithtitle:@"I did it!"
                andMessage:[[NSString alloc] initWithFormat:@"Time required to solve the puzzle: %@",
                            [self formatTimeInterval:self.solveTime]]];
}

- (BOOL) validateValueForGameSqare:(long)tagIndex
                             value:(int)value {
    if (value == 0) {
        return YES;
    }
    
    if (value > 9) {
        return NO;
    }
    
    GameSquare *square = [self.board objectAtIndex:tagIndex];
    if (square.originalValue > 0) {
        return NO;
    }
    
    return ![self checkGameSquare:square atPosition:(unsigned)tagIndex];
}

- (Game*) findGameWithIndex :(long)indx {
    NSArray *games = [Game MR_findAllWithPredicate:
                      [NSPredicate predicateWithFormat:@"index.longValue == %ld", indx]
                                         inContext:[NSManagedObjectContext MR_defaultContext]];
    
    if (games.count == 1) {
        return [games firstObject];
    }
    
    return [games lastObject];
}

- (void) loadThisGame :(long)gameNumber {
    
    self.currentGame = [self findGameWithIndex:gameNumber];
    
    if (self.currentGame) {
        [self.board removeAllObjects];
        
        [self buildGameBoard:[self.currentGame.game componentsSeparatedByString:@","]];
        
        if (![self ValidateGameBoard]) {
            [self removeCurrentGame];
            [self loadRandomlySelectedGame];
        }
        
        self.state = canned;
        [self displayGameDataWithGame:self.currentGame];
        self.startTime = [NSDate date];
    } else {
        self.state = dataError;
        [self doAlertWithtitle:@"Find Error"
                    andMessage:[NSString stringWithFormat:@"Could not find game number %@", [self.numberFormatter stringFromNumber:[[NSNumber alloc] initWithLong:gameNumber]]]];
    }
}

- (void) buildGameBoard:(NSArray*)game {
    for (NSString *s in game) {
        GameSquare *square = [[GameSquare alloc] init];
        square.originalValue = square.currentValue = s.intValue;
        if (square.originalValue > 0) {
            square.digitColor = self.white;
        } else {
            square.digitColor = self.green;
        }
        [self.board addObject:square];
    }
}

- (void) loadRandomlySelectedGame {
    [self loadThisGame:arc4random_uniform((int)[Game MR_countOfEntitiesWithContext:[NSManagedObjectContext MR_defaultContext]])];
}

- (BOOL) ValidateGameBoard {
    BOOL hasError = NO;
    
    unsigned position = 0;
    for (GameSquare *square in self.board) {
        if (square.currentValue > 0) {
            hasError = [self checkGameSquare:square
                                  atPosition:position];
        }
        position++;
    }
    
    if (self.state == clearGame || self.state == invalidBoard) {
        self.state = custom;
    }
    
    if (hasError) {
        self.state = invalidBoard;
    }
    
    return !hasError;
}

- (BOOL) checkGameSquare:(GameSquare*)square
              atPosition:(unsigned)position {
    BOOL hasError = NO;
    if (square.currentValue < 1 || square.currentValue > 9 || ![self noConflictForValue:square.currentValue atPosition:position]) {
        hasError = YES;
        square.digitColor = self.red;
    }
    return hasError;
}

- (void) clearBoard {
    for (GameSquare *square in self.board) {
        square.originalValue = 0;
        square.currentValue = 0;
        square.digitColor = self.green;
    }
    self.state = clearGame;
}

- (int) countOfUnassignedCells:(NSMutableArray*)board {
    int count = 0;
    
    for (GameSquare *square in board) {
        if (square.currentValue == 0) {
            count++;
        }
    }
    return count;
}

- (void) removeCurrentGame {
    if (self.currentGame.index.longValue < [Game MR_countOfEntities]) {
        [Game MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"index.longvalue == %ld", self.currentGame.index.longValue]];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }
}

#pragma mark - persist game

-(void) saveGamesArray {
    int unassignCells = [self countOfUnassignedCells:self.board];

    NSMutableString *str = [[NSMutableString alloc] init];
    for (GameSquare *square in self.board) {
        [str appendFormat:@"%d,", square.currentValue];
    }
    
    [str deleteCharactersInRange:NSMakeRange(str.length - 1, 1)];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int unassignedCellsMax = [[defaults stringForKey:@"unassigned_cells_max"] intValue];
    int unassignedCellsMin = [[defaults stringForKey:@"unassigned_cells_min"] intValue];
    
    if (unassignCells <= unassignedCellsMax &&
        unassignCells >= unassignedCellsMin) {
        Game *tempGame = [Game MR_createEntityInContext:[NSManagedObjectContext MR_defaultContext]];
        tempGame.index = [[NSNumber alloc] initWithLong:[Game MR_countOfEntitiesWithContext:[NSManagedObjectContext MR_defaultContext]] - 1];
        tempGame.game = str;
        tempGame.lastPlayed = nil;
        tempGame.bestTime = @0.0;
        tempGame.bestTimeDate = nil;
        tempGame.bestTimeDevice = nil;
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    } else {
        [self doAlertWithtitle:@"No save."
                    andMessage:[NSString stringWithFormat:@"Number of empty cells must be between %d and %d. There are currently %d.", unassignedCellsMin, unassignedCellsMax, unassignCells]];
    }
}

#pragma mark - puzzle solving logic

-(BOOL) solveNextUnassignedPosition {
    for (unsigned position = 0; position < self.board.count; position++) {
        GameSquare *square = [self.board objectAtIndex:position];
        if (square.currentValue == 0) {
            for (square.currentValue = 1; square.currentValue < 10; square.currentValue++) {
                if ([self noConflictForValue:square.currentValue atPosition:position]) {
                    if ([self solveNextUnassignedPosition]) {
                        return YES;
                    }
                }
            }
            square.currentValue = 0;
            return NO;
        }
    }
    return YES;
}

-(BOOL) noConflictForValue:(unsigned)prospect
                atPosition:(unsigned)position {
    
    return [self checkColumn:prospect position:position] &&
    [self checkRow:prospect atPosition:position] &&
    [self checkSubgrid:prospect prospect:position];
}

- (BOOL) checkColumn:(unsigned int)prospect
            position:(unsigned int)position {
    for (unsigned currentPosition = position % 9; currentPosition < 81; currentPosition += 9) {
        if (currentPosition != position) {
            GameSquare *square = [self.board objectAtIndex:currentPosition];
            if (square.currentValue == prospect) {
                return NO;
            }
        }
    }
    return YES;
}

- (BOOL) checkRow:(unsigned)prospect
       atPosition:(unsigned)position {

    unsigned rowEnd = position / 9 * 9 + 9;
    for (unsigned currentPosition = rowEnd - 9; currentPosition < rowEnd; currentPosition++) {
        if (currentPosition != position) {
            GameSquare *square = [self.board objectAtIndex:currentPosition];
            if (square.currentValue == prospect) {
                return NO;
            }
        }
    }
    return YES;
}

- (BOOL) checkSubgrid:(unsigned int)prospect
             prospect:(unsigned int)position {
    unsigned topLeft = position / 27 * 27 + position % 9 / 3 * 3;
    unsigned bottomRight = topLeft + 21;
    unsigned checkCounter = 1;
    for (unsigned currentPosition = topLeft; currentPosition < bottomRight; currentPosition++) {
        if (currentPosition != position) {
            GameSquare *square = [self.board objectAtIndex:currentPosition];
            if (square.currentValue == prospect) {
                return NO;
            }
        }
        if (checkCounter++ % 3 == 0) {
            currentPosition += 6;
        }
    }
    return YES;
}

#pragma mark - game generator

-(void) createNewGame {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int savedGamesMax = [[defaults stringForKey:@"number_of_saved_games_max"] intValue];
    
    if ([self countOfUnassignedCells:self.board] == 0) {
        if ([Game MR_countOfEntities] < savedGamesMax && [self ValidateGameBoard]) {
            
            switch (arc4random_uniform(4)) {
                case 0:
                    [self keepOneNumber];
                    break;
                    
                case 1:
                    [self randomSelection];
                    break;
                    
                default:
                    [self randomElimination];
                    break;
            }
            
            [self saveGamesArray];
        }
    } else {
        self.selectedTagIndex = -1;
    }
}

-(void) randomElimination {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int unassignedCellsMax = [[defaults stringForKey:@"unassigned_cells_max"] intValue];
    
    for (GameSquare *square in self.board) {
        if (arc4random_uniform(100) > 65 && [self countOfUnassignedCells:self.board] < unassignedCellsMax) {
            square.currentValue = 0;
        }
    }
}

-(void) randomSelection {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int unassignedCellsMax = [[defaults stringForKey:@"unassigned_cells_max"] intValue];
    int unassignedCellsMin = [[defaults stringForKey:@"unassigned_cells_min"] intValue];
    
    unsigned n;
    do {
        n = arc4random_uniform(81) + 1;
    } while (n < unassignedCellsMin || n > unassignedCellsMax);
    
    while ([self countOfUnassignedCells:self.board] > n) {
        GameSquare *square = [self.board objectAtIndex:arc4random_uniform(81)];
        square.currentValue = 0;
    }
}

-(void) keepOneNumber {
    int n = arc4random_uniform(9)+ 1;
    for (GameSquare *square in self.board) {
        if (square.currentValue != n) {
            square.currentValue = 0;
        }
    }
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    self.pickerView.hidden = YES;
    if (self.state == solved && textField.tag < 82) {
        [self doAlertWithtitle:@"Forbidden" andMessage:@"Computer solutions may not be edited."];
        return NO;
    } else {
        self.selectedTagIndex = (int)textField.tag - 1;
        if (self.selectedTagIndex < 81) {
            GameSquare *square = [self.board objectAtIndex:self.selectedTagIndex];
            if (square.originalValue == 0) {
                textField.backgroundColor = self.black;
                self.pickerView.hidden = NO;
                [self.view addSubview:self.pickerView];
                return NO;
            }
        } else if (self.selectedTagIndex == 81) {
            textField.backgroundColor = self.black;
        }
    }
    
    return YES;
}

#pragma nark - textField delegates

-(BOOL) textFieldShouldEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.gameName resignFirstResponder];
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    [self processGameNameTextField];
}

- (void) processGameNameTextField {
    unsigned requestedGameNumber = self.gameName.text.intValue;
    
    if (requestedGameNumber < [Game MR_countOfEntitiesWithContext:[NSManagedObjectContext MR_context]]) {
        [self loadThisGame:requestedGameNumber];
        [self updateDisplay];
    } else {
        [self doAlertWithtitle:@"Invalid Request."
                    andMessage:[NSString stringWithFormat:@"There are only %@ games in storage.",
                                [self.numberFormatter  stringFromNumber:[[NSNumber alloc] initWithUnsignedLong:
                                                                         [Game MR_countOfEntitiesWithContext:[NSManagedObjectContext MR_context]]]]]];
    }
}

#pragma mark - picker logic

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.pickerData.count;
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString*) pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component {
    return self.pickerData[row];
}

- (void) pickerView:(UIPickerView *)pickerView
       didSelectRow:(NSInteger)row
        inComponent:(NSInteger)component {
    if (self.selectedTagIndex >= 0 && self.selectedTagIndex < 81) {
        UITextField *textField = [self.textFields objectAtIndex:self.selectedTagIndex];
        textField.backgroundColor = self.cellBackgroundColor;
        GameSquare *square = [self.board objectAtIndex:self.selectedTagIndex];
        if (square.originalValue == 0) {
            square.currentValue = (unsigned)row;
            if ([self validateValueForGameSqare:self.selectedTagIndex value:square.currentValue]) {
                square.digitColor = self.green;
                if ([self countOfUnassignedCells:self.board] == 0) {
                    [self gameSolved];
                } else {
                    self.state = inProgress;
                }
            } else {
                square.digitColor = self.red;
                self.state = invalidBoard;
            }
            [self updateDisplay];
        } else {
            [self doAlertWithtitle:@"No change."
                        andMessage:@"Only cells that started empty may be changed."];
        }
    } else {
        [self doAlertWithtitle:@"No change."
                    andMessage:@"Please select a target cell first."];
    }
}

- (void) gameSolved {
    self.solveTime = -[self.startTime timeIntervalSinceNow];
    self.state = solved;
    [self updateDisplay];
    
    Game *g = [self findGameWithIndex:self.currentGame.index.longValue];
    g.lastPlayed = [NSDate date];
    if (self.solveTime < g.bestTime.doubleValue) {
        g.bestTime = [[NSNumber alloc] initWithDouble:self.solveTime];
        g.bestTimeDate = g.lastPlayed;
        g.bestTimeDevice = [[UIDevice currentDevice] name];
    }
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    [self doAlertWithtitle:@"You did it!"
                andMessage:[[NSString alloc] initWithFormat:@"Time required to solve the puzzle: %@",
                            [self formatTimeInterval:self.solveTime]]];
    [self displayGameDataWithGame:g];
}

- (NSString*) formatTimeInterval:(NSTimeInterval)timeInterval {
    self.timeIntervalFormatter.allowedUnits = NSCalendarUnitHour |
    NSCalendarUnitMinute |
    NSCalendarUnitSecond;
    self.timeIntervalFormatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehaviorPad;
    return [self.timeIntervalFormatter stringFromTimeInterval:timeInterval];
}

- (void) displayGameDataWithGame :(Game*)g {
    if (g.bestTimeDate) {
        NSString *str = [NSString stringWithFormat:@"Best time: %@\nPlayed on: %@",
                         [self formatTimeInterval:g.bestTime.doubleValue], [self.dateFormatter stringFromDate:g.bestTimeDate]];
        [self doAlertWithtitle:[NSString stringWithFormat:@"Game %ld", g.index.longValue]
                    andMessage:str];
    } else if (self.state != solved) {
        [self doAlertWithtitle:@"Fresh game!" andMessage:@"Complete this game and you're a winner."];
    }
}

- (void) updateDisplay {
    
    switch (self.state) {
        case solved:
            self.gameName.text = @"Game Over";
            self.gameName.textColor = self.green;
            break;

        case canned:
            self.gameName.text = [NSString stringWithFormat:@"Game Number: %ld",
                                  self.currentGame.index.longValue];
            self.gameName.textColor = self.green;
            break;
            
        case saved:
            self.gameName.text = [NSString stringWithFormat:@"Saved as game number: %ld",
                                  self.currentGame.index.longValue];
            self.gameName.textColor = self.green;
            break;
            
        case clearGame:
            self.gameName.text = @"";
            break;
            
        case inProgress:
            self.gameName.text = [self formatTimeInterval:-[self.startTime timeIntervalSinceNow]];
            self.gameName.textColor = self.white;
            break;
            
        case custom:
            self.gameName.text = @"Custom";
            self.gameName.textColor = self.white;
            break;
            
        case tooFewUnassignedCells:
            self.gameName.text = @"NOT ENOUGH EMPTY CELLS";
            self.gameName.textColor = self.red;
            break;
            
        case tooManyUnassignedCells:
            self.gameName.text = @"TOO MANY EMPTY CELLS";
            self.gameName.textColor = self.red;
            break;
            
        case dataError:
            self.gameName.text = @"Database Error";
            self.gameName.textColor = self.red;
            break;

        case invalidBoard:
            self.gameName.text = @"Invalid";
            self.gameName.textColor = self.red;
            break;
    }
    [self updateBoard];
}

-(void) updateBoard {
    self.gameName.backgroundColor = self.cellBackgroundColor;
    unsigned position = 0;
    for (UITextField *textField in self.textFields) {
        textField.backgroundColor = self.cellBackgroundColor;
        textField.highlighted = NO;
        textField.selected = NO;
        GameSquare *square = [self.board objectAtIndex:position];
        if (square.currentValue) {
            textField.text = [self.numberFormatter stringFromNumber:[[NSNumber alloc]
                                                                     initWithUnsignedInt:square.currentValue]];
            textField.textColor = square.digitColor;
        } else {
            textField.text = @"";
            textField.textColor = self.green;
        }
        position++;
    }
}

- (void) doAlertWithtitle:(NSString*)title
               andMessage:(NSString*)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
   
}

- (void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
