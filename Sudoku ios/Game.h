//
//  Game.h
//  
//
//  Created by Philip Huffman on 2015-08-08.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Game : NSManagedObject

@property (nonatomic, retain) NSString * game;
@property (nonatomic, retain) NSNumber * index;
@property (nonatomic, retain) NSDate * lastPlayed;
@property (nonatomic, retain) NSNumber * bestTime;
@property (nonatomic, retain) NSString * bestTimeDevice;
@property (nonatomic, retain) NSDate * bestTimeDate;

@end
